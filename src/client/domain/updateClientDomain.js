const { updateClientInputSchema } = require('../schema/input/updateClientInput');
const { updateClientService } = require('../service/updateClientService');
const { publishUpdateBirth} = require('../service/publishUpdateBirth');
const { updateBirthEvent } = require('../schema/event/updateBirthEvent');

const { calculateAge } = require("../helper/createClient");

const timestamp = new Date().toISOString();

const updateClientDomain = async (commandPayload, commandMeta) => {
    new updateClientInputSchema(commandPayload, commandMeta);

    if (commandPayload.birth) {
        
        if (
            calculateAge(commandPayload.birth) < 18 ||
            calculateAge(commandPayload.birth) > 65
        ) {
            return {
              status: 400,
              body: "Client must be between 18 and 65 years old",
            };
        }

        await publishUpdateBirth(new updateBirthEvent(commandPayload, commandMeta));
        commandPayload.updated_at = timestamp;
        const updatedClient = await updateClientService(commandPayload);
        
        return {
            status: 200,
            body: updatedClient,
        };
    }

    commandPayload.updated_at = timestamp;
    const updatedClient = await updateClientService(commandPayload);

    return {
        status: 200,
        body: updatedClient,
    };

};

module.exports = { updateClientDomain };