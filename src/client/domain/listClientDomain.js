const { listClientInputSchema } = require('../schema/input/listClientInput');
const { listClientService } = require('../service/listClientService');

const listClientDomain = async (commandPayload, commandMeta) => {

    const clients = await listClientService();
    
    return {
        status: 200,
        body: clients,
    };
};

module.exports = { listClientDomain };