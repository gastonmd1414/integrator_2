const { updateGiftService } = require('../service/updateGiftService');

const { giftChoser } = require('../helper/createClient');

const updateGiftDomain = async (eventPayload, eventMeta) => {
  const event = JSON.parse(eventPayload.Message);

  let whatGift = giftChoser(event.birth);

  const params = {
    TableName: process.env.CLIENT_TABLE,
    Key: { "dni": event.dni },
    UpdateExpression: "SET #G = :g",
    ExpressionAttributeNames: { "#G": "gift" },
    ExpressionAttributeValues: { ":g": { S: whatGift } },
    ReturnValues: "ALL_NEW",
  }; 
  
  await updateGiftService(params);  
    
  return {
    status: 200,
    body: "Gift added succesfully",
  };
};

module.exports = { updateGiftDomain };