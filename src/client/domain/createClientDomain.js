const { createClientInputSchema } = require('../schema/input/createClientInput');
const { createClientService } = require('../service/createClientService');
const { publishCreateClient } = require('../service/publishCreateClient');
const { createClientEvent } = require('../schema/event/createClientEvent');

const { calculateAge } = require("../helper/createClient");

const createClientDomain = async (commandPayload, commandMeta) => {
    new createClientInputSchema(commandPayload, commandMeta);

    if (
        calculateAge(commandPayload.birth) < 18 ||
        calculateAge(commandPayload.birth) > 65
    ) {
        return {
          status: 400,
          body: "Client must be between 18 and 65 years old",
        };
    }

    await createClientService(commandPayload);
    await publishCreateClient(new createClientEvent(commandPayload, commandMeta));
    
    return {
        status: 200,
        body: "Client added succesfully",
    };
};

module.exports = { createClientDomain };