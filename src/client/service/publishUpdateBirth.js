const sns = require("ebased/service/downstream/sns");

async function publishUpdateBirth(updateBirthEvent) {

  const { eventPayload, eventMeta } = updateBirthEvent.get();

  const snsPublishParams = {
    TopicArn: process.env.CLIENT_UPDATE_TOPIC,
    Message: eventPayload,
  };
  
  await sns.publish(snsPublishParams, eventMeta);
}

module.exports = { publishUpdateBirth };