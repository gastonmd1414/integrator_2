const dynamo = require('ebased/service/storage/dynamo');

async function createCardService(eventPayload) {
  const params = eventPayload;
  const { Attributes } = await dynamo.updateItem(params);
  return Attributes;
}

module.exports = { createCardService }; 