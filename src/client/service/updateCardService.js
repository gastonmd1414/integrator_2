const dynamo = require('ebased/service/storage/dynamo');

async function updateCardService(eventPayload) {
  const params = eventPayload;
  const { Attributes } = await dynamo.updateItem(params);
  return Attributes;
}

module.exports = { updateCardService }; 