function calculateAge (birthday) {
    // birthday is a string in format YYYYMMDD
    const ageDifMs = Date.now() - new Date(birthday).getTime();
    const ageDate = new Date(ageDifMs);
    return Math.abs(ageDate.getUTCFullYear() - 1970);
  };

function randomNumber(minimum, maximum){
  return Math.round( Math.random() * (maximum - minimum) + minimum);
}

function giftChoser (birthday){
  const month = new Date(birthday).getMonth();
  const day = new Date(birthday).getDate();
  
  let gift
  
  if(month >= 1 && month <= 3 ){
    if (month === 3 && day >= 21){
      gift = 'Buzo'
    } else {
      gift = 'Remera'
    }
  }
  
  if(month >= 4 && month <= 6 ){
    if (month === 6 && day >= 21){
      gift = 'Sweater'
    } else {
      gift = 'Buzo'
    }
  }
  
  if(month >= 7 && month <= 9 ){
    if (month === 9 && day >= 21){
      gift = 'Camisa'
    } else {
      gift = 'Sweater'
    }
  }
  
  if(month >= 10 && month <= 12 ){
    if (month === 12 && day >= 21){
      gift = 'Remera'
    } else {
      gift = 'Camisa'
    }
  }

  return gift
}
  
module.exports = {
  calculateAge,
  randomNumber,
  giftChoser
};