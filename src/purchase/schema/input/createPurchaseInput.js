const { InputValidation } = require('ebased/schema/inputValidation');

class createPurchaseInputSchema extends InputValidation {
    constructor(payload, meta) {
      super({
        source: meta.status,
        payload: payload,
        source: 'PURCHASE.CREATE',
        specversion: 'v1.0.0',
        schema: {
          id: { type: String, required: true },
          productName: { type: String, required: true },
          originalPrice: { type: String, required: true },
        },
      });
    }
};

module.exports = { createPurchaseInputSchema };