function calculatePrice(client, purchase) {
  if(client.creditCard.type === 'Gold') {
      let price = 0;
      const discount = (purchase.originalPrice * 12) / 100;
      price = purchase.originalPrice - discount;
      purchase.finalPrice = price;
      return purchase;
  }
    let price = 0;
    const discount = (purchase.originalPrice * 8) / 100;
    price = purchase.originalPrice - discount;
    purchase.finalPrice = price;
    return purchase;
}

module.exports = {
  calculatePrice
}