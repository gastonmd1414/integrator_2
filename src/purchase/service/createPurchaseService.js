const dynamo = require('ebased/service/storage/dynamo');

const timestamp = new Date().toISOString();

async function createPurchaseService(commandPayload) {
  await dynamo.putItem({
    TableName: process.env.PURCHASE_TABLE,
    Item: {
      pk: 'purchase',
      sk: commandPayload.id,
      'created_at': timestamp,
      'updated_at': '',
      'deleted_at': '',
      M: {
        productname: commandPayload.productname,
        originalPrice: commandPayload.originalPrice,
        finalPrice: commandPayload.finalPrice
      }
    }
  });
}

module.exports = { createPurchaseService };